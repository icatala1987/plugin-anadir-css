<?php
/**
	Plugin Name:	Plugin Personalización CSS Ismael Catalá
	Plugin URI:		http://www.servimatica.es/
	Description:	Plugin para añadir CSS
	Version: 		0.1
	Author:			Ismael Catalá Gil
	Author URI:		http://www.servimatica.es/
	License:		GPLv2 or later
**/

/**
 * Registers a stylesheet.
 */
function wpdocs_register_plugin_styles() {
    wp_register_style( 'icatala_di_plugin', plugins_url( 'icatala_di_plugin/assets/css/icatala.css' ) );
    wp_enqueue_style( 'icatala_di_plugin' );
}
// Register style sheet.
add_action( 'wp_enqueue_scripts', 'wpdocs_register_plugin_styles',90 );
?>